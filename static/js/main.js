Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--)
        if (this[i] === obj)
            return true;
    return false;
};

var myTumblrPhotos = function (data) {
    var max = data.posts.length;
    var selecta = [];
    var rand = 0;
    var COUNT = 6;
    if (COUNT > max) COUNT = max;

    for (var i = 0; i < COUNT; i++) {
        do {
            rand = Math.floor(Math.random() * max);
        } while (selecta.contains(rand));
        selecta[i] = rand;
    }
    var content = '';
    for (var i = 0; i < COUNT; i++) {
        var obj = data.posts[selecta[i]];
        var img = obj['photo-url-100'];
        var url = obj['url-with-slug'];
        var txt = obj['photo-caption'].replace(/<[a-zA-Z\/][^>]*>/g, '') + ' @' + obj['date-gmt'];
        content += ' <a href="' + url + '" title="' + txt + '" target="_blank"><img border="0" alt="' + txt + '" src="' + img + '" valign="middle" /></a>';
    }
    document.getElementById('myTumblrContainer').innerHTML = content;
};

var myTumblrNews = function (data) {
    var tumblrNews = document.getElementById('tumblr-news')
    if (tumblrNews != null) {
        for (var i = 0; i < data.posts.length && i < 8; i++) {
            var dat = data.posts[i]['date-gmt'];
            var url = data.posts[i]['url-with-slug'];
            var tit = data.posts[i]['regular-title'];
            var bod = data.posts[i]['regular-body'];

            var div = document.createElement('div');
            div.className = 'post pure-u-1';
            var html = '<h1 class="content-subhead pure-u-1"><span class="date">@'
            html += dat + '</h1>';
            html += '<header class="post-header"><h2 class="post-title pure-u-1"><a href="'+ url +'" target="_blank">'+ tit +'</a></h2></header>';
            html += '<div class="post-description pure-u-1">' + bod + '</div>';
            div.innerHTML = html;
            tumblrNews.appendChild(div);
        }
    }
}
