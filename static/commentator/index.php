<?php
session_start();
$page  = isset($_GET['page' ])? $_GET['page' ]: 'index';
$title = isset($_GET['title'])? $_GET['title']: 'Inicio';

require('commentator.php');
$comments = new Commentator($page, $title);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="comments.css" type="text/css" media="screen" charset="utf-8" />
    <title>Comentarios de <?php echo $title;?></title>
<script type="text/javascript">
function resizeIframe(iframeID) { 
  if(self==parent) return false; /* Checks that page is in iframe. */ 
  else if(document.getElementById&&document.all) /* Sniffs for IE5+.*/ 
  {}

    var FramePageHeight = document.body.scrollHeight + 10; /* framePage 
                                                          is the ID of the framed page's BODY tag. The added 10 pixels prevent an 
                                                          unnecessary scrollbar. */ 

  parent.document.getElementById(iframeID).style.height=FramePageHeight; 
  /* "iframeID" is the ID of the inline frame in the parent page. */
}

function myResize() {
  resizeIframe('ifComments');
}

window.onload = function () {
  myResize();
  window.setInterval('myResize();', 10000);
}

</script>
  </head>
  <body>
<h2 id="comments">Comentarios (<?php echo $comments->commentcount; ?>)</h2>
<div class="section">
  <?php $comments->write(); ?>
</div>
  </body>
</html>
