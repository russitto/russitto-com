# Matias Russitto
## Senior PHP Developer

> [matias@russitto.com](matias@russitto.com)
> [http://russitto.com](http://russitto.com)
> Mobile: +54 11 50350863

------

### Skills {#skills}

* PHP Programming
  : +15 years experience.

* GNU/Linux user
  : 7 years experience.

* Scrum Master
  : 1 year experience.

-------

### Technical {#technical}

1. PHP
1. MySQL
1. Javascript
1. CSS
1. Linux
1. CoffeeScript
1. Node.js
1. Sass 
1. Jade
1. MongoDB
1. MSSQL
1. Oracle

------

### Experience {#experience}

Qubit.tv
: *Senior PHP developer*
  __2014-2015__
  Developer, Scrum Master

Basso Brovelli
: *Senior PHP developer*
  __2012-2014__
  Developer, SysAdmin

Ambassador Fueguina
: *DevOp*
  __2011__
  SysAdmin, PHP Developer

Freelance
: *Computer Tutor*
  __2008-2010__
  Programming, Linux, Office teacher

Studio Patagonia
: *PHP Developer*
  __2007-2011__

GuiarTDF
: *PHP Developer*
  __2007__

Crystal Solutions
: *Business Intellicence Consultant*
  __2004-2007__
  Reporting and Datawarehousing Analyst/Developer

Editorial Argenta
: *Developer*
  __2003__
  ASP Developer

------

### Education {#education}

Universidad de Tierra del Fuego
: *Licenciatura en Sistemas (unfinished)*
  __2010-2011__

Universidad de Palermo - Seminar
: *Eclipse / IBM: IBM Technical Breafings: Eclipse - IBM Software Development Plataform*
  __2006__

Universidad Tecnológica Nacional
: *Java J2SE: Programación Java (SL-210 Y SL-275)*
  __2005__

Universidad de Buenos Aires
: *Licenciatura en Ciencias de la Computación (unfinished)*
  __1998-2004__
     

------

### Others {#others}

* **Business Intelligence Consultant**
  Clients: Coca-Cola, Prosegur, Sancor Seguros, Sancor Lácteos, Mastercard, Cencosud (6 months in Santiago de Chile)
* **Software Libre Enthusiast**
  KDE, Arch Linux, FLISoL & Vim lover
* **Father in Love**
  [http://vida.russitto.com](http://vida.russitto.com)

------

### Footer {#footer}

Matias Russitto -- [matias@russitto.com](matias@russitto.com) -- [http://russitto.com](http://russitto.com) -- +54 11 50350863

------
