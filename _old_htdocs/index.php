<?php
define('MAGPIE_DIR', 'lib/magpierss/');
define('MAGPIE_INPUT_ENCODING', 'UTF-8');
define('MAGPIE_OUTPUT_ENCODING', 'UTF-8');
require_once(MAGPIE_DIR.'rss_fetch.inc');
$rss=fetch_rss('http://feeds.feedburner.com/ushcompu');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Matias Russitto</title>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
  <div id="header-wrapper">
    <div id="header">
      <div id="logo">
        <h1><a name="tope" href="#tope"><span>Matias</span>Russitto</a></h1>
      </div>
      <div id="menu">
        <ul>
          <li class="current_page_item"><a href="#tope">Inicio</a></li>
          <li><a href="#quien">¿Quién soy?</a></li>
          <li><a href="#trabajo">¿De qué trabajo?</a></li>
          <li><a href="#gustos">¿Qué me gusta?</a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- end #header -->
  <div id="page">
    <div id="content">
      <div class="post">
        <h2 class="title"><a name="quien" href="#quien">¿Quién soy?</a></h2>
        <!-- p class="meta"><span class="author"><a href="#">Someone</a></span> <span class="date">July 07, 2010</span>&nbsp;<span class="links"><a href="#" title="">Comments</a></span></p -->
        <div class="entry">
          <p>Soy <strong>Matias Russitto</strong>, argentino, nacido en el año 79 en Buenos Aires, desde el 2007 resido con mi novia en <a href="http://ushcompu.com.ar/">Ushuaia</a>.</p>
          <p>Actualmente estoy estudiando Licenciatura en Sistemas en la Universidad de Tierra del Fuego.</p>
          <p>Enviame un correo electrónico: <a href="mailto:matias@russitto.com">matias@russitto.com</a></p>
        </div>
      </div>
      <div class="post">
        <h2 class="title"><a name="trabajo" href="#trabajo">¿De qué trabajo?</a></h2>
        <div class="entry">
          <p>
            Soy desarrollador web desde hace mas de 10 años, lo cual acarrea conocer, aprender, y seguir aprendiendo muchas tecnologías, como PHP, SQL, HTML, JavaScript, CSS, servidores web, Unix, GNU/Linux, etc.
          </p>
          <p>
            Y me gustaría aprender mas tecnologías como Python, NoSQL. También sobre hardware, redes y sistemas operativos.
          </p>
          <p>
            Trabajo usando Software Libre, algunos tienen razones morales, otros tienen razones económicas, pero yo tengo principalmente razones tecnológicas. Considero al software libre tecnológicamente superior para el trabajo como programador. Desde fines del 2009 soy parte de <a href="http://slush.com.ar">slush - Software Libre Ushuaia</a>, una iniciativa para acercar estas bondades a la ciudad.
          </p>
        </div>
      </div>
      <div class="post">
        <h2 class="title"><a name="gustos" href="#gustos">¿Qué me gusta?</a></h2>
        <div class="entry last">
          <!--
          <p>
            Voy a caer en la cursilería: mi novia.
          </p>
          -->
          <p>
            Me gusta lo simple, el minimalismo, el principo <a href="http://es.wikipedia.org/wiki/Principio_KISS">KISS</a> y el <a href="http://es.wikipedia.org/wiki/No_te_repitas">DRY</a>. Intento aplicarlo al trabajo y a toda la vida.
          </p>
          <p>
            Pero también, me gustan la música, las películas, y dentro de la computadora me gustan mucho las aplicaciones simples, concisas y sólidas. Algunas de las que me gustan: vim, zsh, hg, feh, apvlv, electricsheep, ssh, mpd, tmux, newsbeuter, irssi, minbif, dwm, wmii, chromium, yii, habari y mas.
            <br />
            Otro conjunto de aplicaciones que me parece excelente e innovador, aunque no simple es <a href="http://kde.org.ar/">KDE</a>.
          </p>
          <p>
            Me gusta mucho *nix (Unix y todos los sistemas tipo Unix), actualmente uso <a href="">Arch Linux</a>, una distribución GNU/Linux enfocada en la simpleza y ligereza. Por eso formo parte de una comunidad de usuarios de esta distro: <a href="http://archlinux.co.cc/">jumanji</a>, es nuestro lugar de encuentro para la distensión y el aprendizaje colaborativo.
          </p>
        </div>
      </div>

<div id="disqus_thread"></div>
<script type="text/javascript">
  /**
    * var disqus_identifier; [Optional but recommended: Define a unique identifier (e.g. post id or slug) for this thread] 
    */
  (function() {
   var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
   dsq.src = 'http://russitto-com.disqus.com/embed.js';
   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
  })();
</script>
<noscript>Activá javascript para ver los <a href="http://disqus.com/?ref_noscript=russitto-com">comentarios</a>.</noscript>

    </div>
    <!-- end #content -->
    <div id="sidebar">
      <ul>
        <li>
          <h2>Buscar</h2>
          <div id="search" >
            <form method="get" action="http://duckduckgo.com/">
              <div>
                <input type="text" name="q" id="search-text" value="" />
                <input type="submit" id="search-submit" value="buscar" />
              </div>
            </form>
          </div>
          <div style="clear: both;">&nbsp;</div>
        </li>
        <li>
          <h2>Enlaces</h2>
          <ul>
            <li><a href="http://archlinux.org/">Arch Linux</a> &lt; Distribución GNU/Linux</li>
            <li><a href="http://www.yiiframework.com/">Yii</a> &lt; Cuadro de trabajo PHP</li>
            <li><a href="http://suckless.org/">suckless</a> &lt; Aplicaciones simples</li>
            <li><a href="http://kde.org/">KDE</a> &lt; Compilación de Software</li>
            <li><a href="http://slush.org/">slush</a> &lt; Software Libre Ushuaia</li>
          </ul>
        </li>
        <li>
        <li id="ushcompu">
          <h2><a href="http://ushcompu.com.ar/">ushcompu</a></h2>
          <ul>
<?php
$i=0;
foreach ($rss->items as $item) {
  if ($i==7) break;
  $href = $item['link'];
  $title = $item['title'];	
  if (strlen($title)>40)
    $title=substr($title,0,37).'...';
  echo '<li><a href="'.$href.'">'.$title.'</a></li>';
  $i++;
}
?>
          </ul>
        </li>
        <li>
          <h2>Gracias</h2>
          <ul>
            <li>por leer todo esto</li>
            <!--
            <li>a mi novia por amarme</li>
            <li>a mis viejos por darme lo mejor</li>
            -->
            <li><a href="#tope">ir al inicio</a></li>
          </ul>
        </li>
        <li>
    <script type="text/javascript" src="http://identi.ca/js/identica-badge.js">
    {
       "user":"ushcompu",
       "server":"identi.ca",
       "headerText":" y amigos",
       "background":"#444444",
       "evenBackground":"#444444"
    }
    </script>
        </li>
      </ul>
    </div>
    <!-- end #sidebar -->
    <div style="clear: both;">&nbsp;</div>
  </div>
  <!-- end #page -->
</div>
<div id="footer">
  <p>
    Copyright <?php echo date('Y');?>. <a href="#tope">Matias Russitto</a>. Ushuaia, Argentina.
    <br />
    Design by Free CSS Templates
    <a href="http://www.freecsstemplates.org/">Free CSS Templates</a>
    Released for free under a Creative Commons Attribution 2.5 License
    <br />
    <a href="#tope">Inicio</a>
  </p>
</div>
<!-- end #footer -->
</body>
</html>
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Perfect Blemish     
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20100729

-->
